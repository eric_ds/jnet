package com.jfirer.jnet.common.buffer.buffer.storage;

import com.jfirer.jnet.common.buffer.arena.Arena;
import com.jfirer.jnet.common.buffer.arena.ChunkListNode;
import com.jfirer.jnet.common.buffer.buffer.BufferType;
import com.jfirer.jnet.common.recycler.Recycler;
import lombok.Getter;

@Getter
public class PooledStorageSegment extends StorageSegment
{
    public static final Recycler<PooledStorageSegment> POOL = new Recycler<>(PooledStorageSegment::new, StorageSegment::setRecycleHandler);
    protected           Arena                          arena;
    protected           ChunkListNode                  chunkListNode;
    protected           long                           handle;

    public void init(Arena arena, ChunkListNode chunkListNode, long handle, int offset, int capacity)
    {
        this.arena         = arena;
        this.chunkListNode = chunkListNode;
        this.handle        = handle;
        init(chunkListNode.memory(), chunkListNode.directChunkAddress(), offset, capacity);
    }

    @Override
    protected void free0()
    {
        arena.free(chunkListNode, handle, capacity);
        arena         = null;
        chunkListNode = null;
        handle        = 0;
        super.free0();
    }

    public StorageSegment makeNewSegment(int newCapacity, BufferType bufferType)
    {
        PooledStorageSegment newSegment = PooledStorageSegment.POOL.get();
        arena.allocate(newCapacity, newSegment);
        return newSegment;
    }
}
