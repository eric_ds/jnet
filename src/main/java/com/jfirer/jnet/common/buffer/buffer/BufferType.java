package com.jfirer.jnet.common.buffer.buffer;

public enum BufferType
{
    HEAP, DIRECT, UNSAFE, MEMORY
}
