package com.jfirer.jnet.common.buffer;

public enum SizeType
{
    TINY, SMALL, NORMAL
}
