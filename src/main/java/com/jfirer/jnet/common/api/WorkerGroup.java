package com.jfirer.jnet.common.api;

public interface WorkerGroup
{
    JnetWorker next();
}
