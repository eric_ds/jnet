package com.jfirer.jnet.common.exception;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * 如果程序自己关闭连接并没有什么异常的时候，就选择这个异常
 *
 * @author eric(eric @ jfire.cn)
 */
public class SelfCloseException extends JnetException
{
    /**
     *
     */
    private static final long serialVersionUID = -5549905347605918610L;

    public SelfCloseException()
    {
        super("代码自己关闭了连接，调用链路为:\r\n" + Arrays.stream(Thread.currentThread().getStackTrace()).map(StackTraceElement::toString).collect(Collectors.joining("\r\n")));
    }

    public SelfCloseException(Throwable cause)
    {
        super("代码自己关闭了连接，调用链路为:\r\n" + Arrays.stream(Thread.currentThread().getStackTrace()).map(StackTraceElement::toString).collect(Collectors.joining("\r\n")), cause);
    }
}
