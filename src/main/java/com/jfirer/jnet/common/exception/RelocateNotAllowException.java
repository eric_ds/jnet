package com.jfirer.jnet.common.exception;

public class RelocateNotAllowException extends JnetException
{
    public RelocateNotAllowException(String msg)
    {
        super(msg);
    }
}
